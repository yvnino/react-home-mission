import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import GoogleMapReact from 'google-map-react';
import Select from 'react-select';
import { Button } from 'react-bootstrap';
import PlacesAutocomplete, { geocodeByAddress, getLatLng } from 'react-places-autocomplete';
import Toolbar from '../components/TopToolBar.jsx';
import GenericList from '../components/GenericList.jsx';
import * as LocationsActions from '../actions/locationActions.js';
import { fetchCategories } from '../actions/categoriesActions.js';
import 'react-select/dist/react-select.css';

class CreateLocationsScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            coordinates: {
                lat: 25.7616798,
                lng: -80.19179020000001
            },
            address: '',
            selectedCategory: null,
            selectBoxValue: []
        };
    }

    componentDidMount() {
        if (this.props.match.params.id) {
            this.props.actions.getLocationById(this.props.match.params.id);
        } else {
            this.props.actions.codeLatLng(this.state.coordinates.lat, this.state.coordinates.lng);
        }
        if (!this.props.categoriesList.length)
            this.props.actions.fetchCategories();
    }

    componentWillUnmount() {
        this.props.actions.getLocationById(null);
    }

    componentWillReceiveProps(nextProps) {
        if (JSON.stringify(this.props.geoLatLng) != JSON.stringify(nextProps.geoLatLng)) {
            let newCoords = {
                lat: nextProps.geoLatLng.lat,
                lng: nextProps.geoLatLng.lng
            };
            this.setState({ coordinates: { ...newCoords } });
        }
        if (this.props.geoAdderess != nextProps.geoAdderess) {
            this.setState({ address: nextProps.geoAdderess });
        }
        if (!this.props.selectedLocation && nextProps.selectedLocation) {
            let tempCtg = nextProps.selectedLocation.category.map(item => {
                return { value: item, label: item.name };
            })
            let state = {
                name: nextProps.selectedLocation.name,
                coordinates: {
                    lat: nextProps.selectedLocation.coordinates.lat,
                    lng: nextProps.selectedLocation.coordinates.lng
                },
                address: nextProps.selectedLocation.address,
                selectBoxValue: tempCtg
            }
            this.setState({ ...state });
        }
    }

    onInputChange = (e) => {
        this.props.actions.checkExistingName(e.target.value, 'Locations');
        this.setState({ name: e.target.value });
    }

    onChangeAddress = (address) => {
        this.setState({ address });
        this.props.actions.geocodeByAddressAction(address);
    }

    updatePoint = (point) => {

        console.log('point', point);
    }

    setCoordinates = (data) => {
        let newCoords = {
            lat: (data.lat) ? data.lat : data.center.lat,
            lng: (data.lng) ? data.lng : data.center.lng,
        };
        this.setState(Object.assign(this.state, { coordinates: newCoords }));
        // this.renderMarkers(null, null);
        this.props.actions.codeLatLng(newCoords.lat, newCoords.lng);
    }
    onCancel = () => {
        let state = {
            name: '',
            coordinates: {
                lat: 59.95,
                lng: 30.33
            },
            address: '',
            selectedCategory: null
        };
        this.setState(Object.assign(this.state, state));
        this.props.history.push("/locations");
    }

    onSave = () => {
        let newCtg = this.state.selectBoxValue.map(item => {
            let itm = item.value;
            return { ...itm };
        });
        let newLoc = {
            name: this.state.name,
            coordinates: { ...this.state.coordinates },
            address: this.state.address,
            category: newCtg,
        };
        if (this.props.selectedLocation) {
            newLoc.id = this.props.selectedLocation.id;
            this.props.actions.editLocation(newLoc);
        }
        else
            this.props.actions.addLocation(newLoc);
        this.props.history.push("/locations");
    }

    handleChangeSelectBox = (e) => {
        this.setState(Object.assign(this.state, { selectBoxValue: e }));
        console.log(e);
    }

    renderMarkers = (map, maps) => {
        let marker = new google.maps.Marker({
            position: {
                lat: this.state.coordinates.lat,
                lng: this.state.coordinates.lng
            },
            map: this.refs.myMap.map_,
            title: 'Hello World!'
        });
    }

    render() {
        const inputProps = {
            value: this.state.address,
            onChange: this.onChangeAddress,
        }
        const selectBoxOpt = this.props.categoriesList.map(item => {
            if (!this.state.selectBoxValue.includes(item))
                return { value: { ...item }, label: item.name };
        })
        return (
            <div className='createLocationPage'>
                <h1>Create Location</h1>
                <div className='pageWrapper' style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly' }}>
                    <div style={{ display: 'flex', flexDirection: 'column' }}>
                        <div style={{ display: 'flex', flexDirection: 'row' }}>
                            <div className='leftPanel' style={{ padding: '10px', display: 'flex', flexDirection: 'column', alignSelf: 'center' }}>
                                <label>Address</label>
                                <PlacesAutocomplete inputProps={inputProps} onSelect={this.onChangeAddress} onClick={() => console.log('e')} />
                                <div className='formField' style={{ display: 'flex', flexDirection: 'column' }} >
                                    <label>Coordinates</label>
                                    <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
                                        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}><label>Lat</label><div style={{ marginLeft: '4px' }}>{this.state.coordinates.lat}</div></div>
                                        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between', marginLeft: '4px' }}><label>Lng</label><div style={{ marginLeft: '4px' }}>{this.state.coordinates.lng}</div></div>
                                    </div>
                                </div>

                            </div>
                            <div className='formPanel' style={{ padding: '10px', alignSelf: 'center' }}>
                                <div className="form-group" style={{ display: 'flex', flexDirection: 'column' }}>
                                    <div className='formField' style={{ display: 'flex', flexDirection: 'column' }}>
                                        <label>Location Name</label>
                                        <input type="text" id="username" className="form-control" value={this.state.name} onChange={this.onInputChange} placeholder='Please Enter Location name' />
                                    </div>
                                    <div className='formField'>
                                        <label>Categories</label>
                                        <Select
                                            multi={true}
                                            value={this.state.selectBoxValue}
                                            onChange={this.handleChangeSelectBox}
                                            options={selectBoxOpt}
                                            removeSelected={true}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='btnField'>
                            <Button className="pointer" color="primary" onClick={this.onSave} disabled={(this.state.coordinates.lat && this.state.coordinates.lng && this.state.selectBoxValue.length && this.state.address !== '' && this.state.name !== '' && !this.props.nameExisted) ? false : true}>Save</Button>{' '}
                            <Button className="pointer" color="secondary" onClick={this.onCancel}>Cancel</Button>
                        </div>
                    </div>
                    <div className='rightPanel' style={{ padding: '10px', display: 'flex', flexDirection: 'column' }}>
                        <label>Map View</label>
                        <div className='map'>
                            <GoogleMapReact
                                ref='myMap'
                                bootstrapURLKeys={{ key: 'AIzaSyBm1w-IAVQtAo02K8R1iNDsNq2LyV62mWE' }}
                                center={[this.state.coordinates.lat, this.state.coordinates.lng]}
                                zoom={12}
                                onClick={this.setCoordinates}
                            />
                        </div>
                    </div>
                </div>

            </div>
        )
    }

}

function mapStateToProps(state) {
    const categoriesList = Array.from(state.categoriesReducer['categoriesList']);
    const locationsList = Array.from(state.locationsReducer['locationsList']);
    const geoLatLng = { ...state.locationsReducer['geoLatLng'] };
    const geoAdderess = state.locationsReducer['geoAdderess'];
    const selectedLocation = state.locationsReducer['selectedLocation'];
    const nameExisted = state.locationsReducer['nameExisted'];



    return {
        categoriesList,
        locationsList,
        geoLatLng,
        geoAdderess,
        selectedLocation,
        nameExisted
    }
}


function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({ ...LocationsActions, fetchCategories }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateLocationsScreen);

