import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as CategoriesActions from '../actions/categoriesActions.js';
import Toolbar from '../components/TopToolBar.jsx';
import FormModal from '../components/FormModal.jsx';
import GenericList from '../components/GenericList.jsx';

class CategoriesScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            modalOpen: false,
            objToEdit: null
        };
    }

    editClick(item) {
        this.setState({ objToEdit: { ...item } });
        this.toggleModal();
    }

    renderList = () => {
        let ctgList = <GenericList
            listType='category'
            list={this.props.categoriesList}
            deleteItem={(itemId) => this.deleteCategory(itemId)}
            editItemParams={(item) => this.editClick(item)}
            toSort={false}
        />
        return ctgList;
    }

    componentDidMount() {
        this.props.actions.fetchCategories();
    }

    addCategory = (ctgObj) => {
        this.toggleModal(false);
        this.props.actions.addCategory({ ...ctgObj })
    }

    deleteCategory = (itemId) => {
        this.props.actions.deleteCategory(itemId)
    }

    editCategory = (ctgObj) => {
        this.toggleModal(false);
        let editedObj = Object.assign(this.state.objToEdit, ctgObj);
        this.props.actions.editCategory(editedObj);
        this.setState({ objToEdit: null });
    }

    toggleModal = (bool) => {
        this.setState({ modalOpen: (bool) ? bool : !this.state.modalOpen });
    }

    closeModal = () => {
        this.setState({ objToEdit: null });
        this.toggleModal();
    }

    render() {
        return (
            <div>
                <Toolbar page='category' addAction={() => this.toggleModal(true)} title='My Categories' />
                <div className='listItems'>
                    {this.renderList()}
                </div>
                <FormModal formType='category' modal={this.state.modalOpen} objToEdit={this.state.objToEdit} onCreate={this.addCategory} onEdit={this.editCategory} onClose={() => this.closeModal()} />
            </div>
        )
    }
}

function mapStateToProps(state) {
    const categoriesList = Array.from(state.categoriesReducer['categoriesList']);

    return {
        categoriesList
    }
}


function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({ ...CategoriesActions }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoriesScreen);