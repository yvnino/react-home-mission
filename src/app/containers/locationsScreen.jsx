import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Toolbar from '../components/TopToolBar.jsx';
import GenericList from '../components/GenericList.jsx';
import FormModal from '../components/FormModal.jsx';
import * as LocationsActions from '../actions/locationActions.js';
import { fetchCategories } from '../actions/categoriesActions.js';

class LocationsScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            modalOpen: false,
            objToShow: null,
            detailsType: '',
            toSortList: false,
            selectedGroups: []
        };
    }

    componentDidMount() {
        this.props.actions.fetchLocations();
        if (!this.props.categoriesList.length)
            this.props.actions.fetchCategories();
    }

    toggleModal = (bool) => {
        this.setState({ modalOpen: (bool) ? bool : !this.state.modalOpen });
    }

    showDetails = (detailsType, item) => {
        if (navigator.vibrate) {
            navigator.vibrate(200);
        } 
        this.setState({ objToShow: { ...item }, detailsType });
        this.toggleModal();
    }


    renderGroupedList = () => {
        let groupedList = this.state.selectedGroups.map((item, index) => {
            //check which locations has that category
            let singleList = this.props.locationsList.filter((locItem, ind) => {
                for (let i = 0; i < locItem.category.length; i++) {
                    if (locItem.category[i].id === item.value.id) {
                        return locItem;
                    }
                }
            })
            //prepare sub lists
            return (
                <div key={index} style={{padding:'40px'}}>
                    <label>{item.label}</label>
                    {(!singleList.length) ? <div>No Locations with that Category</div> : <GenericList
                        listType='location'
                        list={singleList}
                        deleteItem={(itemId) => this.props.actions.deleteLocation(itemId)}
                        editItemParams={(item) => this.editClick(item)}
                        openModal={this.showDetails}
                    />}
                </div>
            );
        });
        return groupedList;
    }

    renderList = (group = null) => {
        if (this.state.selectedGroups.length) {
            return this.renderGroupedList();
        }
        let list = <GenericList
            listType='location'
            list={this.props.locationsList}
            deleteItem={(itemId) => this.props.actions.deleteLocation(itemId)}
            editItemParams={(item) => this.editClick(item)}
            openModal={this.showDetails}
            toSort={this.state.toSortList}
        />
        return list;
    }

    editClick = (item) => {
        this.props.history.push("/location/" + item.id);
    }

    onAddClick = () => {
        this.props.history.push("/create-locations");
    }

    onGrpSelect = (ctgObj) => {
        this.setState({ selectedGroups: [...ctgObj] });
    }

    render() {
        return (
            <div>
                <Toolbar page='location' handleGoupedCategory={this.onGrpSelect} categoryList={this.props.categoriesList} addAction={this.onAddClick} title='My Locations' onSortHandler={() => this.setState({ toSortList: !this.state.toSortList })} />
                <div className='listItems'>
                    {this.renderList()}
                </div>
                <FormModal page='location' detailsType={this.state.detailsType} modal={this.state.modalOpen} showDetails={this.state.objToShow} onClose={() => this.setState({ modalOpen: false })} />
            </div>
        )
    }

}

function mapStateToProps(state) {
    const categoriesList = Array.from(state.categoriesReducer['categoriesList']);
    const locationsList = Array.from(state.locationsReducer['locationsList']);

    return {
        categoriesList,
        locationsList
    }
}


function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators({ ...LocationsActions, fetchCategories }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(LocationsScreen);

