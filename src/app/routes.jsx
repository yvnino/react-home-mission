import React from 'react';
import { Route, Switch, Redirect} from 'react-router-dom';
import App from './components/App/App.jsx';
import Toaster from './components/Toaster.js';
import CategoriesScreen from './containers/categoriesScreen.jsx';
import LocationsScreen from './containers/locationsScreen.jsx';
import CreateLocationsScreen from './containers/CreateLocationsScreen.jsx';
import FooterBar from './components/FooterBar.jsx';

import css from './style/app.css';

class Router extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {

    let currentUrlPath = window.location.pathname;
    if(currentUrlPath === '/')
      return <Redirect to="/categories" />
    return (

      <div className="Router">
        <div className='appContainer'>
            <Route path="/" component={App} />
            <Route path="/categories" component={CategoriesScreen} />
            <Route path="/locations" component={LocationsScreen} />
            <Route path="/location/:id" component={CreateLocationsScreen} />
            <Route path="/create-locations" component={CreateLocationsScreen} />
        </div>
        <Toaster />
        <div className='footerElm'>
            <FooterBar />
        </div>
      </div>
    );

  }
}
export default Router;