const LocationsReducer = (state = { locationsList: [] , geoAdderess:'' , geoLatLng : null, selectedLocation:null }, action) => {
    
        switch (action.type) {
            case 'CREATE_LOCATION_SUCCESS': {
                return Object.assign({}, state, {
                    locationsList: [...state.locationsList, action.data]
                })
            }
            case 'GET_LOCATION_BY_ID_SUCCESS': {
                return Object.assign({}, state, {
                    selectedLocation: (!action.data) ? null : {...action.data}
                })
            }
            case 'FETCH_LOCATIONS_SUCCESS': {
                return Object.assign({}, state, {
                    locationsList: [...action.data]
                })
            }case 'GOOGLE_GET_LATLNG_SUCCESS': {
                return Object.assign({}, state, {
                    geoLatLng: {...action.data}
                })
            }
            case 'GOOGLE_GET_ADDRESS_SUCCESS': {
                return Object.assign({}, state, {
                    geoAdderess: action.data
                })
            }
            default:
                return state;
        }
    }
    
    export default LocationsReducer;