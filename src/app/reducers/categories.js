const categoriesReducer = (state = { categoriesList: [] }, action) => {

    switch (action.type) {
        case 'CREATE_CATEGORY_SUCCESS': {
            return Object.assign({}, state, {
                categoriesList: [...state.categoriesList, action.data]
            })
        }
        case 'FETCH_CATEGORIES_SUCCESS': {
            return Object.assign({}, state, {
                categoriesList: [...action.data]
            })
        }
        default:
            return state;
    }
}

export default categoriesReducer;