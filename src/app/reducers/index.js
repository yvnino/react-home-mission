import { combineReducers } from 'redux';
// import loginReducer from './loginReducer';
// import settingsReducer from './settingsReducer';
// import viewsReducer from './viewsReducer';
import toasterReducer from './notifications.js';
import categoriesReducer from './categories.js';
import locationsReducer from './locations.js';

// import googleReducer from './googleReducer';


const mainReducer = combineReducers({
    toasterReducer,
    categoriesReducer,
    locationsReducer
})

export default mainReducer