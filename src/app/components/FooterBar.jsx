import React, { Component } from 'react';
import { Link, Redirect } from 'react-router-dom';
import Ionicon from 'react-ionicons';
// import { connect } from 'react-redux';

import css from '../style/navbar.css';

class FooterBar extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <div className="nav-bar" style={{paddingLeft:'40px' , paddingRight:'40px'}}>
          <nav>
            <Link to='/categories'><div title='My Categories'><Ionicon icon="ios-pricetag-outline" color="#fff" fontSize="30px" className="pointer" /></div></Link>
            <Link to='/locations'><div title='My Locations'><Ionicon icon="ios-pin-outline" color="#fff" fontSize="30px" className="pointer" /></div></Link>
          </nav>
        </div>
      </div>
    );
  }

}


export default FooterBar;