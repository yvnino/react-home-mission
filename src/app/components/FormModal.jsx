import React, { Component } from 'react';
import { Button, Modal} from 'react-bootstrap';
import GoogleMapReact from 'google-map-react';

class FormModal extends React.Component {
    constructor(props) {
        super(props);
        this.state= {
            nameInput : ''
        }
    }

    onInputChange = (e)=>{
        this.setState({nameInput: e.target.value});
    }

    componentWillUnmount(){
        this.setState({nameInput : ''});

    }

    // componentDidMount() {
    //     document.getElementById("okBtn")
    //         .addEventListener("keyup", function (event) {
    //             event.preventDefault();
    //             if (event.keyCode === 13) {
    //                 document.getElementById("okBtn").click();
    //             }
    //         });
    // }

    componentWillReceiveProps(nextProps) {
        if(nextProps.objToEdit){
            this.setState({nameInput : nextProps.objToEdit.name});
        }
    }

    createCategory = () =>{
        if(this.state.nameInput.length){
            this.props.onCreate({name : this.state.nameInput});
            this.setState({nameInput : ''});
        }
    }
    onCancel = ()=>{
        this.setState({nameInput : ''});
        this.props.onClose();
    }

    onEdit = () =>{
        if(this.state.nameInput.length){
            this.props.onEdit({name : this.state.nameInput});
            this.setState({nameInput : ''});
        }

    }

    renderMarkers=(map, maps)=>{
        let marker = new maps.Marker({
            position: { 
                lat: this.props.showDetails.coordinates.lat, 
                lng: this.props.showDetails.coordinates.lng
            },
            map,
            title: 'Hello World!'
          });
    }

    renderModalBody = () => {
        if(this.props.detailsType === 'map'){
            return <div className='map'>
            <GoogleMapReact
                ref='myMap'
                bootstrapURLKeys={{ key: 'AIzaSyBm1w-IAVQtAo02K8R1iNDsNq2LyV62mWE' }}
                center={[this.props.showDetails.coordinates.lat, this.props.showDetails.coordinates.lng]}
                zoom={12}
                onGoogleApiLoaded={({map, maps}) => this.renderMarkers(map, maps)}
            />
        </div>
        }else{
            return(
                <div>
                <div >
                    <div className="form-group">
                        <div className='formField'>
                            <label>Location Name</label>
                            <div>{this.props.showDetails.name}</div>
                        </div>
                        <div className='formField'>
                            <label>Address</label>
                            <div>{this.props.showDetails.address}</div>
                        </div>
                        <div className='formField'>
                            <label>Coordinates</label>
                            <div>{this.props.showDetails.coordinates.lat} </div>
                            <div>{this.props.showDetails.coordinates.lng} </div>
                        </div>
                        <div className='formField'>
                            <label>Categories</label>
                            {<div>{this.props.showDetails.category.map((item, index) => {
                                return <div key={index}>{item.name} </div>
                            })} </div>}
                        </div>
                    </div>
                </div>
            </div>
            )
        }

    }

    render() {
        if(this.props.page ==='location' && this.props.showDetails){
            return (
                <Modal show={this.props.modal} >
                    <Modal.Header closeButton> Location {this.props.detailsType.substr(0, 1).toUpperCase() + this.props.detailsType.substr(1)}</Modal.Header>
                    <Modal.Body >
                        {this.renderModalBody()}
                    </Modal.Body>
                    <Modal.Footer >
                        <Button className="pointer" color="secondary" onClick={this.onCancel}>Close</Button>
                </Modal.Footer>
            </Modal>
            )
        }

        return (
            <div className="FormModal">
                <Modal show={this.props.modal} >
                    <Modal.Header closeButton> Create Category</Modal.Header>
                    <Modal.Body >
                        <div className="form-group">
                            <div className='formField'>
                                <label>Category Name</label>
                                <input type="text" id="username" className="form-control" value={this.state.nameInput} onChange={this.onInputChange} placeholder='Please Enter Category name' />
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer >
                        <Button className="pointer" color="primary" onClick={(this.props.objToEdit) ? this.onEdit : this.createCategory}>Save</Button>{' '}
                        <Button className="pointer" color="secondary" onClick={this.onCancel}>Cancel</Button>
                    </Modal.Footer>
                </Modal>
            </div>
        )
    }
}

export default FormModal;