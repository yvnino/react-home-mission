
import React, { Component } from 'react';
import Ionicon from 'react-ionicons';

import css from '../style/list.css';

class GenericList extends React.Component {
    constructor(props) {
        super(props)
    }

    renderLocationFields = (item) => {
        let elm = <td>
            <div className="list-details">
                <div className='pointerBtn' title="Definition" onClick={() => this.props.openModal('definition', item)}> <Ionicon icon="ios-information-circle-outline" color="#d9534f" fontSize="30px" className="pointer" /></div>
                <div className='pointerBtn' title="Map View" onClick={() => this.props.openModal('map', item)}> <Ionicon icon="ios-map" color="#f0ad4e" fontSize="35px" className="pointer" /></div>
            </div>
        </td>
        return elm;
    }

    render() {

        if (!this.props.list.length)
            return <div className="GenericList bounceInUp animated" style={{padding:'40px'}}>No Items - Please create items</div>
            
        let copiedList = Array.from(this.props.list);
        let list = (this.props.toSort) ? copiedList.sort(function (a, b) {
            if (a.name < b.name) return -1;
            if (a.name > b.name) return 1;
            return 0;
        }) : copiedList;
        return (

            <div className="GenericList bounceInUp animated">
                <table className="table table-bordered user-table ">
                    <thead>
                        <tr>
                            <th id="th-id">#</th>
                            <th>Name</th>
                            {this.props.listType === 'location' && <th className="th-actions">Details</th>}
                            <th className="th-actions">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {list.map((item, index) => {
                            return (
                                <tr key={item.id} >
                                    <td>{index + 1}</td>
                                    <td>{item.name}</td>
                                    {this.props.listType === 'location' && this.renderLocationFields(item)}
                                    <td>
                                        <div className='list-details'>
                                            <div className='pointerBtn' title="Delete" onClick={() => this.props.deleteItem(item.id)}><Ionicon icon="ios-trash" color="#d9534f" fontSize="30px" className="pointer" /></div>
                                            <div className='pointerBtn' title="Edit" onClick={() => this.props.editItemParams(item)}><Ionicon icon="md-create" color="#f0ad4e" fontSize="35px" className="pointer" /></div>
                                        </div>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default GenericList;