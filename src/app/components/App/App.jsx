import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import Ionicon from 'react-ionicons';
import bootsrap from 'bootstrap/dist/css/bootstrap.css';

class App extends React.Component {

  constructor(props, context) {
    super(props, context);
  }

  render() {
    return (
      <div style= {{display:'flex' , flexDirection:'row' , marginLeft:'40px'}}>
        <Ionicon icon="ios-locate-outline" color="#5cb85c" fontSize="35px" className="pointer" /><h4>{'Sample React App'}</h4>
      </div>
    );
  }
}

export default App;