import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Ionicon from 'react-ionicons';
import Select from 'react-select';
// import { connect } from 'react-redux';

import css from '../style/toolbar.css';

class Toolbar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectBoxValue: [],
      openSelect:false
    }
    // this.clickLogOut = this.clickLogOut.bind(this);
  }

onAddClick= () => {
    this.props.addAction();
}

onSortClick =() =>{
  this.props.onSortHandler();

}

renderSelectCtg = ()=>{
  
  this.setState(Object.assign(this.state, { openSelect: true}));
}


handleChangeSelectBox = (e) =>{
  this.props.handleGoupedCategory(e);
  this.setState(Object.assign(this.state, { selectBoxValue: e , openSelect:false}));
}

render() {
  if(this.props.page === 'location'){
    //prepare selectBox values
    var selectBoxOpt = this.props.categoryList.map(item => {
      if (!this.state.selectBoxValue.includes(item))
        return { value: { ...item }, label: item.name };
    })
    selectBoxOpt.unshift({value:{name:'all' , id:'0'}, label:'All'});
  }
    return (
      <div className="NavView">
        <nav className='toolbar'>
          <div className="nav-bar-title">{this.props.title}</div>
          <div className="nav-bar-btn" >
            <div onClick={this.onAddClick} title={(this.props.page === 'location') ? 'Add Location' : 'Add Category'} className='topBarBtn'>
              <Ionicon icon="ios-add-circle" color="#5cb85c" fontSize="35px" className="pointer" />
            </div>
            {this.props.page === 'location' && <div onClick={this.onSortClick} title='Sort A-Z' className='topBarBtn'>
              <Ionicon icon="ios-funnel" color="#5cb85c" fontSize="35px" className="pointer" />
            </div>}
            {this.props.page === 'location' && <div onClick={this.renderSelectCtg} title='By Category' className='topBarBtn' style={{ display: 'flex', flexDirection: 'row' }}>
              <Ionicon icon="logo-buffer" color="#5cb85c" fontSize="35px" className="pointer" />
              {this.state.openSelect &&<div style={{width: '200px'}}> <Select
                multi={true}
                value={this.state.selectBoxValue}
                onChange={this.handleChangeSelectBox}
                options={selectBoxOpt}
                removeSelected={true}
              /></div>}
            </div>}
          </div>
        </nav>
      </div>
    );
  }

}


export default Toolbar;