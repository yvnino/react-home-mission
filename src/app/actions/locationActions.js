import { geocodeByAddress, getLatLng } from 'react-places-autocomplete';
import { GenerateMsgUUID } from '../utils.js';

export const createLocationSuccess = (data) => ({
    type: 'CREATE_LOCATION_SUCCESS',
    data
});

export const fetchLocationsListSuccess = (data) => ({
    type: 'FETCH_LOCATIONS_SUCCESS',
    data
});


export const createLocationFail = (e) => ({
    type: 'CREATE_LOCATION_FAIL',
    e
});

//---------TOASTER---------//
export const toasterSuccess = (data) => ({
    type: 'SUCCESS_MESSAGE', data
});

export const toasterFail = (e) => ({
    type: 'FAIL_MESSAGE', e
});

export const getAdressSuccess = (data) => ({
    type: 'GOOGLE_GET_ADDRESS_SUCCESS',
    data
});

export const getAdressFail = (e) => ({
    type: 'GOOGLE_GET_ADDRESS_FAIL',
    e
});

export const getLatLngSuccess = (data) => ({
    type: 'GOOGLE_GET_LATLNG_SUCCESS',
    data
});

export const getLatLngFail = (data) => ({
    type: 'GOOGLE_GET_LATLNG_FAIL',
    data
});

export const getLocationByIdSuccess = (data) => ({
    type: 'GET_LOCATION_BY_ID_SUCCESS',
    data
});

export const checkExistingNameResult = (data) =>({
    type: 'EXIST_NAME_RESULT',
    data
})


export const codeLatLng = (lat, lng) => (dispatch) => {
    var geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({
      'latLng': latlng
    }, function (results, status) {
      if (status === google.maps.GeocoderStatus.OK) {
        if (results[1]) {
            dispatch(getAdressSuccess(results[1].formatted_address));
        } else {
          alert('No results found');
        }
      } else {
        alert('Geocoder failed due to: ' + status);
      }
    });
  }


export const geocodeByAddressAction = (address) => (dispatch) => {
    geocodeByAddress(address)
    .then(results => getLatLng(results[0]))
    .then(latLng => {
        dispatch(getLatLngSuccess(latLng));
    }).catch(error => getAdressSuccess(error));
}


export function addLocation(location) {
    return function (dispatch) {
        let toSave = {...location};
        toSave.id = GenerateMsgUUID();
        let storedLocation = localStorage.getItem('Locations');
        //check if existed Categories item in localStorage
        let oldList = (storedLocation) ? JSON.parse(storedLocation) : [];
        localStorage.setItem('Locations',JSON.stringify([...oldList , toSave]));
        dispatch(createLocationSuccess(toSave));
        dispatch(toasterSuccess({msg: 'Location Added'}));        
    };
};

export function fetchLocations() {
    return function (dispatch) {
        let storedLocation = localStorage.getItem('Locations');
        //check if existed Locations item in localStorage
        let currentList = (storedLocation) ? JSON.parse(storedLocation) : [];
        dispatch(fetchLocationsListSuccess(currentList));
    };
};

export function checkExistingName(name , modelType) {
    return function (dispatch) {
        let storedModel = localStorage.getItem(modelType);
        let currentList = (storedModel) ? JSON.parse(storedModel) : [];
        let result = false;
        for (let i = 0; i < currentList.length; i++) {
            let element = currentList[i];
            if(element.name === name){
                result = true;
                break;
            }
        }
        dispatch(checkExistingNameResult(result));
    };
};

export function editLocation(editedItem) {
    return function (dispatch) {
        let storedLocation = JSON.parse(localStorage.getItem('Locations'));
        //return new list without the removed item
        let newLocationList = storedLocation.map(item =>{
            if(item.id === editedItem.id)
                return editedItem;
            return item;
        });
        //check if existed Locations item in localStorage
        localStorage.setItem('Locations',JSON.stringify([...newLocationList]));
        dispatch(fetchLocationsListSuccess(newLocationList));
        dispatch(toasterSuccess({msg: 'Location Changed Successfuly'})); 
    };
};

export function deleteLocation(id) {
    return function (dispatch) {
        let storedLocation = JSON.parse(localStorage.getItem('Locations'));
        //return new list without the removed item
        let newLocationList = storedLocation.filter(item =>{
            if(item.id !== id)
                return item;
        });
        //check if existed Locations item in localStorage
        localStorage.setItem('Locations',JSON.stringify([...newLocationList]));
        dispatch(fetchLocationsListSuccess(newLocationList));
        dispatch(toasterSuccess({msg: 'Category Deleted'})); 
    };
};

export function getLocationById(id) {
    return function (dispatch) {
        if(!id){
            dispatch(getLocationByIdSuccess(null));
        }else{
            let storedLocation = JSON.parse(localStorage.getItem('Locations'));
            //return new list without the removed item
            let locationList = storedLocation.filter(item =>{
                if(item.id === id)
                    return item;
            });
            //check if existed Locations item in localStorage
            // localStorage.setItem('Locations',JSON.stringify([...newLocationList]));
            dispatch(getLocationByIdSuccess(locationList[0]));
        }
    };
};

