import { GenerateMsgUUID } from '../utils.js';

export const createCategorySuccess = (data) => ({
    type: 'CREATE_CATEGORY_SUCCESS',
    data
});

export const fetchCategoriesListSuccess = (data) => ({
    type: 'FETCH_CATEGORIES_SUCCESS',
    data
});


export const createCategoryFail = (e) => ({
    type: 'CREATE_CATEGORY_FAIL',
    e
});

//---------TOASTER---------//
export const toasterSuccess = (data) => ({
    type: 'SUCCESS_MESSAGE', data
});

export const toasterFail = (e) => ({
    type: 'FAIL_MESSAGE', e
});

var updateLocationsItems = (status, ctgItem) => {
    let storedLocation = localStorage.getItem('Locations');
    let parsedList = (storedLocation) ? JSON.parse(storedLocation) : [];
    let updatedList = parsedList.map(item => {
        if (status === 'edit') {
            let category = item.category.map(ctg => {
                if (ctg.id === ctgItem.id) {
                    return ctgItem
                } else {
                    return ctg;
                }
            })
            item.category = category;
            return item;
        } else {
            let category = item.category.filter(ctg => {
                if (ctg.id !== ctgItem.id)
                    return ctg;
            })
            item.category = category;
            return item;
        }
    })
    localStorage.setItem('Locations',JSON.stringify(updatedList));
}

export function addCategory(ctg) {
    return function (dispatch) {
        let toSave = {...ctg};
        toSave.id = GenerateMsgUUID();
        let storedCtg = localStorage.getItem('Categories');
        //check if existed Categories item in localStorage
        let oldList = (storedCtg) ? JSON.parse(storedCtg) : [];
        localStorage.setItem('Categories',JSON.stringify([...oldList , toSave]));
        dispatch(createCategorySuccess(toSave));
        dispatch(toasterSuccess({msg: 'Category Added'}));        
    };
};

export function deleteCategory(id) {
    return function (dispatch) {
        let storedCtg = JSON.parse(localStorage.getItem('Categories'));
        //return new list without the removed item
        let newCtgList = storedCtg.filter(item =>{
            if(item.id !== id){
                return item;
            }else{
                updateLocationsItems('remove',item);
            }
        });
        //check if existed Categories item in localStorage
        localStorage.setItem('Categories',JSON.stringify([...newCtgList]));
        dispatch(fetchCategoriesListSuccess(newCtgList));
        dispatch(toasterSuccess({msg: 'Category Deleted'})); 
    };
};

export function editCategory(editedItem) {
    return function (dispatch) {
        let storedCtg = JSON.parse(localStorage.getItem('Categories'));
        //return new list without the removed item
        let newCtgList = storedCtg.map(item =>{
            if(item.id === editedItem.id){
                updateLocationsItems('edit',editedItem);
                return editedItem;
            }
            return item;
        });
        //check if existed Categories item in localStorage
        localStorage.setItem('Categories',JSON.stringify([...newCtgList]));
        dispatch(fetchCategoriesListSuccess(newCtgList));
        dispatch(toasterSuccess({msg: 'Category Changed Successfuly'})); 
    };
};

export function fetchCategories() {
    return function (dispatch) {
        let storedCtg = localStorage.getItem('Categories');
        //check if existed Categories item in localStorage
        let currentList = (storedCtg) ? JSON.parse(storedCtg) : [];
        dispatch(fetchCategoriesListSuccess(currentList));
    };
};